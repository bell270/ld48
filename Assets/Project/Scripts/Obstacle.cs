using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    public int type;
    public float speed;
    public Vector2 _moveDirection;
    [SerializeField] private float _movementSmoothing = .05f;
    private Vector3 _velocityRef = Vector3.zero;
    private bool _spawnedNext = false;

    private Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        if (_rb == null)
        {
            _rb = gameObject.AddComponent<Rigidbody2D>();
        }
        _rb.gravityScale = 0;

        _moveDirection = Vector2.zero;
        if (type == 0)
        {
            _moveDirection = Vector2.up;
        }
        if (type == 1)
        {
            _moveDirection = Vector2.up;
        }
        if (type == 2)
        {
            _moveDirection = Vector2.up;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Vector3 targetVelocity = _moveDirection * speed;
        _rb.velocity = Vector3.SmoothDamp(_rb.velocity, targetVelocity, ref _velocityRef, _movementSmoothing);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "GameOverTrigger")
        {
            Destroy(gameObject);
        }
        if (type == 0 && collision.tag == "Respawn" && !_spawnedNext)
        {
            _spawnedNext = true;
            GetComponentInParent<LevelManager>().Respawn(type);
        }
    }
}
