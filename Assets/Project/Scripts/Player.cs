using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Player : MonoBehaviour
{
    public bool isPaused = false;

    [Header("Movement")]
    Rigidbody2D _rb;
    private Vector2 _movement;
    [SerializeField] private float _speed = 10f;
    [SerializeField] private float _movementSmoothing = .05f;
    private Vector3 _velocityRef = Vector3.zero;

    [Header("Bubble")]
    public GameObject ownBubble;
    public GameObject bubblePopPrefab;
    public bool _isBubbled = false;
    private float _bubbleSpeed;
    private Vector2 _bubbleDirection;
    private float _bubbleTimer;

    [Header("Other")]
    public LevelManager levelManager;
    public Animator animator;
    public LineRenderer _lr;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        ownBubble.SetActive(false);
    }

    void Update()
    {
        animator.SetFloat("Speed", _rb.velocity.magnitude);

        _lr.SetPosition(1, transform.position);

        if (!isPaused)
        {
            if (!_isBubbled)
            {
                _movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * _speed;

                if (Input.GetMouseButtonDown(0))
                {
                    //FindObjectOfType<AudioManager>().PlayClip("Shot");
                }
                if (Input.GetMouseButtonDown(1))
                {
                    //OnDeath("Button");
                }
            }
            else
            {
                Vector3 targetVelocity = _bubbleDirection * _bubbleSpeed;
                _rb.velocity = Vector3.SmoothDamp(_rb.velocity, targetVelocity, ref _velocityRef, 0f);
                _bubbleTimer -= Time.fixedDeltaTime;
                if (_bubbleTimer <= 0f)
                {
                    _isBubbled = false;
                    Color newColor = GetComponent<SpriteRenderer>().color;
                    newColor.a = 1f;
                    GetComponent<SpriteRenderer>().color = newColor;
                    ownBubble.SetActive(false);
                    FindObjectOfType<AudioManager>().PlayClip("pop2");
                    ParticleSystem.MainModule pop = Instantiate(bubblePopPrefab, transform.position, Quaternion.identity).GetComponent<ParticleSystem>().main;
                    StartCoroutine(FindObjectOfType<CameraShake>().Shake(.15f, .4f));
                    pop.startColor = Color.blue;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
                _ = SceneManager.UnloadSceneAsync(1);
            }
            else
            {
                isPaused = true;
                Time.timeScale = 0;
                SceneManager.LoadScene(1, LoadSceneMode.Additive);
            }

        }
    }

    private void FixedUpdate()
    {
        Vector3 targetVelocity = _movement * Time.fixedDeltaTime;
        _rb.velocity = Vector3.SmoothDamp(_rb.velocity, targetVelocity, ref _velocityRef, _movementSmoothing);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "GameOverTrigger" || collision.tag == "Respawn")
        {
            OnDeath(collision.tag);
        }
        if (collision.tag == "Bubble")
        {
            _rb.velocity = new Vector3(_rb.velocity.x, 0f);
            Obstacle bubble = collision.GetComponent<Obstacle>();
            _bubbleSpeed = bubble.speed;
            _bubbleDirection = bubble._moveDirection;
            _bubbleTimer = 5f;
            ownBubble.SetActive(true);
            _isBubbled = true;
            Color newColor = GetComponent<SpriteRenderer>().color;
            newColor.a = 0.5f;
            GetComponent<SpriteRenderer>().color = newColor;
            Instantiate(bubblePopPrefab, bubble.transform.position, Quaternion.identity);
            FindObjectOfType<AudioManager>().PlayClip("pop1");
            Destroy(bubble.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Medusa")
        {
            FindObjectOfType<AudioManager>().PlayClip("bzzz");
        }
    }

    private void OnDeath(string deathTag)
    {
        FindObjectOfType<AudioManager>().PlayClip("dead");
        Time.timeScale = 0;
        isPaused = true;
        string deadBy = "";
        switch (deathTag)
        {
            case "Respawn":
                deadBy = "You drowned";
                break;
            case "GameOverTrigger":
                deadBy = "You stuck";
                break;
            default:
                deadBy = "You stopped";
                break;
        }
        PlayerPrefs.SetString("LastDeathCause", deadBy);
        PlayerPrefs.SetInt("FinalScore", levelManager.score);
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1, LoadSceneMode.Additive);
    }
}
