using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{

    public IEnumerator Shake(float duration, float magnitude)
    {
        CinemachineVirtualCamera virtualCamera = GetComponent<CinemachineVirtualCamera>();
        CinemachineBasicMultiChannelPerlin perlin = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        
        float elapsed = 0f;
        while (elapsed < duration)
        {
            perlin.m_AmplitudeGain = Mathf.Lerp(magnitude, 0f, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        perlin.m_AmplitudeGain = 0;
    }
}
