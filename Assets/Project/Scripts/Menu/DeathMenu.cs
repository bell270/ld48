using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    public TextMeshProUGUI DeathCause;
    public TextMeshProUGUI FinalScore;
    private void Start()
    {
        string finalScore = "You are " + PlayerPrefs.GetInt("FinalScore", 0).ToString() + "m deep";
        if (PlayerPrefs.GetInt("NewHighscore", 0) == 1)
        {
            finalScore += "\nNew highscore!";
        }
        else
        {
            finalScore += "\nhighscore " + PlayerPrefs.GetInt("Highscore", 0).ToString();
        }
        PlayerPrefs.SetInt("NewHighscore", 0);

        DeathCause.text = PlayerPrefs.GetString("LastDeathCause", "BAD LUCK");
        FinalScore.text = finalScore;
    }
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    public void GoToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
