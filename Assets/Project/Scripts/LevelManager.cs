using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LevelManager : MonoBehaviour
{
    public bool started = false;
    [Header("Walls")]
    public GameObject WallPrefab;
    private float _wallsLastPosition = 0f;
    public LayerMask wallLayer;

    [Header("Bubbles")]
    public GameObject BubblePrefab;
    private float _bubbleSpawnTimer = 0f;

    [Header("Medusa")]
    public GameObject MedusaPrefab;
    private float _medusaSpawnTimer = 0f;

    [Header("Score")]
    public int score;
    private int scoreMax = 100;
    private float _scoreTimer = 0f;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highscoreText;

    [Header("Background")]
    public Light2D GlobalLight;

    void Start()
    {
        _wallsLastPosition = 0f;
        scoreText.text = score.ToString();
        highscoreText.text = GetHighscore().ToString();
    }

    private void Update()
    {
        if (!started) return;
        _medusaSpawnTimer -= Time.deltaTime;
        _bubbleSpawnTimer -= Time.deltaTime;

        if (_medusaSpawnTimer <= 0f)
        {
            MedusaSpawner();
        }
        if(_bubbleSpawnTimer <= 0f && score > 20)
        {
            BubbleSpawner();
        }
        GlobalLight.intensity = score > 150 ? 0.05f : System.Math.Max(1 - ((float)score / scoreMax), 0.1f);
    }

    private void FixedUpdate()
    {
        if (!started) return;
        _scoreTimer += Time.fixedDeltaTime;
        if(_scoreTimer >= 1f)
        {
            score++;
            scoreText.text = score.ToString();
            highscoreText.text = GetHighscore().ToString();
            _scoreTimer = 0f;
        }
    }

    public void Respawn(int type)
    {
        if(type == 0)
        {
            WallSpawner();
        }
    }

    public void WallSpawner()
    {
        float rand = Random.Range(-1f, 1f);
        GameObject newWall = Instantiate(WallPrefab, new Vector3(_wallsLastPosition + rand, -12.5f, 0), Quaternion.identity);
        newWall.transform.SetParent(transform);
        newWall.layer = (int)Mathf.Log(wallLayer.value, 2);
        newWall.tag = "Wall";
    }

    public void BubbleSpawner()
    {
        float rand = Random.Range(-6f, 6f); 
        GameObject newBubble = Instantiate(BubblePrefab, new Vector3(rand, -12, 0), Quaternion.identity);
        newBubble.transform.SetParent(transform);
        newBubble.tag = "Bubble";
        newBubble.GetComponent<Obstacle>().speed = 2f + Random.Range(-1f, 1f);
        _bubbleSpawnTimer = Random.Range(1f, 5f);
    }

    public void MedusaSpawner()
    {
        float rand = Random.Range(-6f, 6f) + _wallsLastPosition; 
        int amount = Random.Range(1, 5);
        for (int i = 0; i < amount; i++)
        {
            GameObject newMedusa = Instantiate(MedusaPrefab, new Vector3((rand - amount/2f + i), -12, 0), Quaternion.identity);
            newMedusa.transform.SetParent(transform);
            newMedusa.tag = "Medusa";
            newMedusa.GetComponent<Obstacle>().speed = 1f;
        }
        _medusaSpawnTimer = Random.Range(1f, 5f);
    }

    public int GetHighscore()
    {
        int highscoreCurrent = PlayerPrefs.GetInt("Highscore", 0);
        if (score > highscoreCurrent)
        {
            highscoreCurrent = score;
            PlayerPrefs.SetInt("Highscore", highscoreCurrent);
            PlayerPrefs.SetInt("NewHighscore", 1);
        }
        return highscoreCurrent;
    }
}
